/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * main.c:
 * DESCRIPION:
 *    Entry point of program execution.
 *    The program first reads a text file provided by the user. Every word is
 *    parsed into a hash table data structure. Afterwards, the first word of
 *    every line present on stdin is parsed, which will subsequently be searched
 *    in the hash table. If the word has occurred in the provided text file, the
 *    line number of every occurrence is printed.
 *
 * USAGE:
 *    "lookup <file>" Where file is a text document. While running the command,
 *    provide words on stdin which should be looked up.
 *    -t, Perform timing tests, using different parameters for building hash
 *        tables.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "array.h"
#include "hash_func.h"
#include "hash_table.h"

#define LINE_LENGTH 256

// Values chosen by timing tests in timed_construction().
#define TABLE_START_SIZE 4096
#define MAX_LOAD_FACTOR 0.8
#define HASH_FUNCTION hash_maximal_io

#define START_TESTS 7
#define MAX_TESTS 4
#define HASH_TESTS 4

/* Replace every non-ascii char with a space and lowercase every char. */
static void cleanup_string(char *line) {
  for (char *c = line; *c != '\0'; c++) {
    *c = (char)tolower(*c);
    if (!isalpha(*c)) {
      *c = ' ';
    }
  }
}

/* Return a pointer to a heap allocated string with all the characters we
 * use as word delimiters. Return NULL on failure. */
static char *calc_delim(void) {
  const size_t ascii_table_size = 128;
  char *res = malloc(sizeof(char) * ascii_table_size);
  if (!res) {
    return NULL;
  }

  int res_index = 0;
  for (unsigned char c = 1; c < ascii_table_size; c++) {
    if (!isalpha(c)) { /* if it's not an alpha it's a delimiter char. */
      res[res_index++] = (char)c;
    }
  }
  res[res_index] = '\0';
  return res;
}

// Parse words from lines present in text file into hash table.
//
// fp: Text file to parse lines from.
// delims: String of delimiter characters separating words.
// hash_table: The table to parse words of lines into.
//
// Returns: 0 if parsing was successful or 1 on failure.
static int parse_lines_words(FILE *fp, char *delims, struct table *hash_table) {
  char *line = malloc(LINE_LENGTH * sizeof(char));
  if (line == NULL) {
    return 1;
  }

  int line_number = 1;
  while (fgets(line, LINE_LENGTH, fp)) {
    cleanup_string(line);

    for (char *word = ""; word != NULL;) {
      word =
          strcmp(word, "") == 0 ? strtok(line, delims) : strtok(NULL, delims);
      if (word == NULL) {
        break;
      }

      int err = table_insert(hash_table, word, line_number);
      if (err != 0) {
        free(line);
        return 1;
      }
    }
    line_number++;
  }

  free(line);
  return 0;
}

/* Creates a hash table with a word index for the specified file and
 * parameters. Return a pointer to hash table or NULL if an error occured.
 */
static struct table *
create_from_file(char *filename, unsigned long start_size, double max_load,
                 unsigned long (*hash_func)(unsigned char *)) {
  FILE *fp = fopen(filename, "r");
  if (fp == NULL)
    return NULL;

  struct table *hash_table = table_init(start_size, max_load, hash_func);
  if (hash_table == NULL) {
    fclose(fp);
    return NULL;
  }

  char *delims = calc_delim();
  if (delims == NULL) {
    return NULL;
  }

  int err = parse_lines_words(fp, delims, hash_table);
  if (err != 0) {
    table_cleanup(hash_table);
    free(delims);
    fclose(fp);
    return NULL;
  }

  fclose(fp);
  free(delims);
  return hash_table;
}

// Print line numbers for every occurrence of word.
//
// a: The array of line numbers a word occurred on.
// word: The word that was present on said lines.
void print_results(struct array *a, char *word) {
  printf("%s\n", word);
  for (unsigned long i = 0; i < array_size(a); ++i) {
    printf("* %d\n", array_get(a, i));
    if (array_get(a, (i + 1)) == -1) {
      printf("\n");
    }
  }
}

/* Reads words from stdin and prints line lookup results per word.
 * Return 0 if succesful and 1 on failure. */
static int stdin_lookup(struct table *hash_table) {
  char *line = malloc(LINE_LENGTH * sizeof(char));
  if (line == NULL) {
    return 1;
  }

  char *delims = calc_delim();
  if (delims == NULL) {
    return 1;
  }

  while (fgets(line, LINE_LENGTH, stdin)) {
    cleanup_string(line);
    char *word = strtok(line, delims);
    if (word == NULL) {
      free(delims);
      return 1;
    }

    struct array *a = table_lookup(hash_table, word);
    if (a == NULL) {
      continue;
    }

    print_results(a, word);
  }
  free(delims);
  free(line);
  return 0;
}

static void timed_construction(char *filename) {
  /* Here you can edit the hash table testing parameters: Starting size,
   * maximum load factor and hash function used, and see the the effect
   * on the time it takes to build the table.
   * You can edit the tested values in the 3 arrays below. If you change
   * the number of elements in the array, change the defined constants
   * at the top of the file too, to change the size of the arrays. */
  unsigned long start_sizes[START_TESTS] = {2,     256,    4096,  16384,
                                            65536, 131072, 262144};
  double max_loads[MAX_TESTS] = {0.2, 0.4, 0.8, 1.0};
  unsigned long (*hash_funcs[HASH_TESTS])(unsigned char *) = {
      hash_too_simple, hash_java_stdlib, hash_maximal_io, SuperFastHash};

  for (int i = 0; i < START_TESTS; i++) {
    for (int j = 0; j < MAX_TESTS; j++) {
      for (int k = 0; k < HASH_TESTS; k++) {
        clock_t start = clock();
        struct table *hash_table = create_from_file(
            filename, start_sizes[i], max_loads[j], hash_funcs[k]);
        clock_t end = clock();

        printf("Start: %ld\tMax: %.1f\tHash: %d\t -> Time: %ld "
               "microsecs\n",
               start_sizes[i], max_loads[j], k, end - start);
        table_cleanup(hash_table);
      }
    }
  }
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("usage: %s text_file [-t]\n", argv[0]);
    return EXIT_FAILURE;
  }

  if (argc == 3 && !strcmp(argv[2], "-t")) {
    timed_construction(argv[1]);
  } else {
    struct table *hash_table = create_from_file(argv[1], TABLE_START_SIZE,
                                                MAX_LOAD_FACTOR, HASH_FUNCTION);
    if (hash_table == NULL) {
      printf("An error occurred creating the hash table, exiting..\n");
      return EXIT_FAILURE;
    }

    if (stdin_lookup(hash_table) != 0) {
      table_cleanup(hash_table);
      return EXIT_FAILURE;
    }
    table_cleanup(hash_table);
  }

  return EXIT_SUCCESS;
}
