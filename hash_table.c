/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * hash_table.c:
 * DESCRIPION:
 *    Hash table definitions, for hash table operations such as: creating,
 *    inserting, deleting, resizing, looking up and cleaning.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "hash_func.h"
#include "hash_table.h"

int table_resize(struct table *t);
void node_cleanup(struct node *n);

struct table {
  /* The (simple) array used to index the table */
  struct node **array;
  /* The function used for computing the hash values in this table */
  unsigned long (*hash_func)(unsigned char *);

  /* Maximum load factor after which the table array should be resized */
  double max_load_factor;
  /* Capacity of the array used to index the table */
  unsigned long capacity;
  /* Current number of elements stored in the table */
  unsigned long load;
};

/* Note: This struct should be a *strong* hint to a specific type of hash table
 * You may implement other options, if you can build them in such a way they
 * pass all tests. However, the other options are generally harder to code. */
struct node {
  /* The string of characters that is the key for this node */
  char *key;
  /* A resizing array, containing the all the integer values for this key */
  struct array *value;

  /* Next pointer */
  struct node *next;
};

// Create a new node. Appends single integer value to beginning of node's array.
// Node's array capacity is initialized to 8 based on timing tests. Where a
// lower- or higher value would impact performance.
//
// key: The key of the node.
// value: The value of the node.
//
// Returns: Node object on success or NULL on failure.
struct node *node_create(char *key, int value) {
  struct node *n = malloc(sizeof(struct node));
  if (n == NULL) {
    return NULL;
  }

  n->key = malloc(sizeof(char) * (strlen(key) + 1));
  strcpy(n->key, key);

  n->value = array_init(8);
  if (array_append(n->value, value) != 0) {
    free(n);
    return NULL;
  }
  n->next = NULL;

  return n;
}

// Create new hash table and initialize members.
//
// capacity: The number of buckets.
// max_load_factor: The threshold load factor when the hash table should be
//                  resized.
// hash_func: The hash function used to derive bucket indexes from.
//
// Returns: Initialized hash table object or NULL on failure.
struct table *table_init(unsigned long capacity, double max_load_factor,
                         unsigned long (*hash_func)(unsigned char *)) {
  struct table *t = malloc(sizeof(struct table));
  if (t == NULL) {
    return NULL;
  }

  t->array = calloc(capacity, sizeof(struct node *));
  if (t->array == NULL) {
    free(t);
    return NULL;
  }

  t->capacity = capacity;
  t->hash_func = hash_func;
  t->load = 0;
  t->max_load_factor = max_load_factor;

  return t;
}

// Insert key with associated value into hash table. Resizes table buckets if
// the table's max load factor is reached.
//
// t: Table to insert into.
// key: The key to insert into table.
// value: The value of given key.
//
// Returns: 0 on successful insertion or 1 on failure.
int table_insert(struct table *t, char *key, int value) {
  if (table_load_factor(t) > t->max_load_factor && table_resize(t) != 0) {
    return 1;
  }

  size_t index = t->hash_func((unsigned char *)key) % t->capacity;
  struct node *n = t->array[index];
  if (n == NULL) {
    struct node *new = node_create(key, value);
    if (new == NULL) {
      return 1;
    }

    t->array[index] = new;
    t->load++;
  } else {
    while (n != NULL) {
      if (strcmp(n->key, key) == 0) {
        array_append(n->value, value);
        t->load++;
        break;
      }

      if (n->next != NULL) {
        n = n->next;
        continue;
      }

      struct node *new = node_create(key, value);
      if (new == NULL) {
        return 1;
      }
      n->next = new;
      t->load++;
      break;
    }
  }
  return 0;
}

// Insert a node into hash table. "Simple" insert, because resizing or checking
// for collisions is skipped. Meant to be used in conjunction with table_resize.
//
// t: The table to insert node into.
// new_node: Node to insert into table.
//
// Returns: 0 on successful insertion or 1 on failure.
int table_simple_insert_node(struct table *t, struct node *new_node) {
  if (t == NULL || new_node == NULL) {
    return 1;
  }

  size_t index = t->hash_func((unsigned char *)new_node->key) % t->capacity;
  struct node *existing_node = t->array[index];
  if (existing_node == NULL) {
    t->array[index] = new_node;
    t->load++;
  } else {
    new_node->next = existing_node;
    t->array[index] = new_node;
    t->load++;
  }

  return 0;
}

// Get array of ints linked to a key in hash table.
//
// t: Table to search in.
// key: Key to search for.
//
// Returns: NULL if t is NULL or if key is not found. Integer array if key was
//          found.
struct array *table_lookup(struct table *t, char *key) {
  if (t == NULL) {
    return NULL;
  }

  size_t index = t->hash_func((unsigned char *)key) % t->capacity;
  struct node *cur_node = t->array[index];
  while (cur_node != NULL) {
    if (strcmp(cur_node->key, key) == 0) {
      return cur_node->value;
    }
    cur_node = cur_node->next;
  }

  return NULL;
}

// Get current load factor of hash table.
//
// t: The table to get load factor of.
//
// Returns: Current load factor of table or 0 if "t" is null. 0 is assumed as
// an empty table has zero load.
double table_load_factor(struct table *t) {
  return t == NULL ? 0 : (double)t->load / (double)t->capacity;
}

// Delete key and associated value from hash table.
//
// t: The table to delete key from.
// key: The key to delete.
//
// Returns: 0 on successful delete or 1 either on failure or if key does not
//          exist.
int table_delete(struct table *t, char *key) {
  if (t == NULL) {
    return 1;
  }

  size_t index = t->hash_func((unsigned char *)key) % t->capacity;
  struct node *n = t->array[index];
  struct node *node_before = n;
  while (n != NULL) {
    if (strcmp(n->key, key) != 0) {
      node_before = n;
      n = n->next;
      continue;
    }

    if (node_before == n) {
      t->array[index] = n->next;
    } else {
      node_before->next = n->next;
    }

    node_cleanup(n);
    return 0;
  }
  return 1;
}

// Double table bucket size. Rehashes all nodes indexes to account for new
// capacity.
//
// t: The table to resize.
//
// Returns: 0 on successful resize or 1 on failure.
int table_resize(struct table *t) {
  if (t == NULL) {
    return 1;
  }

  unsigned long new_capacity = t->capacity * 2;
  struct table *new_table =
      table_init(new_capacity, t->max_load_factor, t->hash_func);
  if (new_table == NULL) {
    return 1;
  }

  for (unsigned long i = 0; i < t->capacity; ++i) {
    struct node *n = t->array[i];
    if (n == NULL) {
      continue;
    }

    while (n != NULL) {
      struct node *node_after = n->next;
      n->next = NULL; // SLL of nodes is different in new hash table.
      if (table_simple_insert_node(new_table, n) != 0) {
        return 1;
      }
      n = node_after;
    }
  }

  free(t->array);
  t->array = new_table->array;
  t->capacity = new_table->capacity;
  t->load = new_table->load;
  // Table_cleanup would free new_table->array, which must be preserved.
  free(new_table);

  return 0;
}

// Cleanup all (sub-)members of node from memory.
//
// n: The node to clean.
void node_cleanup(struct node *n) {
  if (n == NULL) {
    return;
  }

  array_cleanup(n->value);
  n->value = NULL;
  free(n->key);
  n->key = NULL;
  free(n);
  n = NULL;
}

// Cleanup all (sub-)members of hash table from memory.
//
// t: The table to clean.
void table_cleanup(struct table *t) {
  for (unsigned long i = 0; i < t->capacity; ++i) {
    struct node *n = t->array[i];
    if (n == NULL) {
      continue;
    }

    while (n != NULL) {
      struct node *node_after = n->next;
      node_cleanup(n);
      n = node_after;
    }
  }

  free(t->array);
  t->array = NULL;
  free(t);
  t = NULL;
}
