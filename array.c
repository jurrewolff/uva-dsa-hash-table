/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * array.c:
 * DESCRIPION:
 *    Dynamic array definitions, for operations such as: creating,
 *    appending, deleting, resizing, getting and cleaning.
 */

#include <stdlib.h>

#include "array.h"

struct array {
  int *elems;
  size_t capacity;
  size_t allocated;
};

// Double array capacity.
//
// a: The array to resize.
//
// Returns: 0 on succes or -1 on failure.
int array_resize(struct array *a) {
  if (a == NULL) {
    return -1;
  }

  a->capacity = a->capacity <= 0 ? 16 : a->capacity * 2;
  a->elems = realloc(a->elems, sizeof(int) * a->capacity);
  if (a->elems == NULL) {
    return -1;
  }

  return 0;
}

// Initialize new array with default member values.
//
// initial_capacity: Initial capacity of array.
//
// Returns: Initialized array or NULL when memory allocation fails.
struct array *array_init(unsigned long initial_capacity) {
  struct array *a = malloc(sizeof(struct array));
  if (a == NULL) {
    return NULL;
  }

  a->elems = malloc(sizeof(int) * initial_capacity);
  if (a->elems == NULL) {
    free(a);
    return NULL;
  }

  a->capacity = initial_capacity;
  a->allocated = 0;
  return a;
}

// Clean array from memory.
//
// a: The array to clean.
void array_cleanup(struct array *a) {
  free(a->elems);
  a->elems = NULL;
  free(a);
  a = NULL;
}

// Get value of specific index from array.
//
// a: The array to operate on.
// index: The index to get from array.
//
// Returns: Value of array or -1 on error.
int array_get(struct array *a, unsigned long index) {
  if (a == NULL || index > (a->allocated - 1)) {
    return -1;
  }

  return a->elems[index];
}

// Append value to end of array.
//
// a: The array to append to.
// elem: The value to append.
//
// Returns: 0 on successful append or 1 on failure.
int array_append(struct array *a, int elem) {
  if (a == NULL) {
    return 1;
  }

  if (a->capacity == a->allocated) {
    int err = array_resize(a);
    if (err != 0) {
      return 1;
    }
  }

  a->elems[a->allocated++] = elem;
  return 0;
}

// Get number of elements in array.
//
// a: Array to get number of elements from.
//
// Returns: Number of elements present in array or 0 if a is null. 0 is assumed
// as a non-existing array technically has size 0.
unsigned long array_size(struct array *a) {
  if (a == NULL) {
    return 0;
  }
  return a->allocated;
}
