/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * hash_func.c:
 * DESCRIPION:
 *    Hashing algorithms, that can be used to generate hashes from char arrays.
 */

#include <stdint.h>
#include <string.h>

// For "SuperFastHash".
#undef get16bits
#define get16bits(d) (*((const uint16_t *)(d)))
#if !defined(get16bits)
#define get16bits(d)                                                           \
  ((((uint32_t)(((const uint8_t *)(d))[1])) << 8) +                            \
   (uint32_t)(((const uint8_t *)(d))[0]))
#endif

/* Do not edit this function, as it used in testing too
 * Add you own hash functions with different headers instead. */
unsigned long hash_too_simple(unsigned char *str) {
  return (unsigned long)*str;
}

unsigned long hash_java_stdlib(unsigned char *str) {
  unsigned long h = 0;
  for (int i = 0; str[i] != '\0'; i++) {
    h = h * 31 + str[i];
  }
  return h;
}

unsigned long hash_maximal_io(unsigned char *str) {
  unsigned long h = 0;
  for (unsigned long i = 0; str[i] != '\0'; i++) {
    h = (h << 5) - h + str[i];
  }
  return h;
}

unsigned long SuperFastHash(unsigned char *data) {
  unsigned long len = strlen((char *)data);
  unsigned long hash = len, tmp;
  unsigned long rem;

  if (len <= 0 || data == NULL)
    return 0;

  rem = len & 3;
  len >>= 2;

  /* Main loop */
  for (; len > 0; len--) {
    hash += get16bits(data);
    tmp = (get16bits(data + 2) << 11) ^ hash;
    hash = (hash << 16) ^ tmp;
    data += 2 * sizeof(uint16_t);
    hash += hash >> 11;
  }

  /* Handle end cases */
  switch (rem) {
  case 3:
    hash += get16bits(data);
    hash ^= hash << 16;
    hash ^= (unsigned long)((data[sizeof(uint16_t)]) << 18);
    hash += hash >> 11;
    break;
  case 2:
    hash += get16bits(data);
    hash ^= hash << 11;
    hash += hash >> 17;
    break;
  case 1:
    hash += (unsigned char)*data;
    hash ^= hash << 10;
    hash += hash >> 1;
  }

  /* Force "avalanching" of final 127 bits */
  hash ^= hash << 3;
  hash += hash >> 5;
  hash ^= hash << 4;
  hash += hash >> 17;
  hash ^= hash << 25;
  hash += hash >> 6;

  return hash;
}
