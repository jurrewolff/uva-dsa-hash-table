/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * hash_func.h:
 * DESCRIPION:
 *    Interface for different hashing algorithms, that can be used to generate
 *    hashes from char arrays.
 */

/* Example hash function with terrible performance */
unsigned long hash_too_simple(unsigned char *str);

/* Add the header for your own added hash functions here. You may search online
 * for existing solutions for hashing function, as long as you as you
 * attribute the source, meaning links to the used material.  */

// Java stdlib hashing algorithm provided by lecture slides:
// https://canvas.uva.nl/courses/25440/files/5646725?module_item_id=1114898
//
unsigned long hash_java_stdlib(unsigned char *str);

// Hashing algorithm provided by maximal.io, who got theirs from java
// implementation. The difference is the use of a combination of '<<' and '-'
// operators instead of a single '*'.
// https://datastructures.maximal.io/hash-tables/
unsigned long hash_maximal_io(unsigned char *str);

// Hashing algorithm by Paul Hsieh. To fit the function signature most of the
// efficiency is probably lost.
// http://www.azillionmonkeys.com/qed/hash.html
unsigned long SuperFastHash(unsigned char *data);
